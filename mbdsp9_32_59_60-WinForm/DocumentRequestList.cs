﻿using mbdsp9_32_59_60_WinForm.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mbdsp9_32_59_60_WinForm
{
    public partial class DocumentRequestList : Form
    {
        public DocumentRequestList()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tablePanel_Paint(object sender, PaintEventArgs e)
        {
            DocumentRequest documentRequest1 = new DocumentRequest { Id = "1", DocumentType = "Type 1", RequestingUser = "User 1", RequestDate = "22/07/2023", RequestStatus = "En cours", Action = "Valider" };
            DocumentRequest documentRequest2 = new DocumentRequest { Id = "2", DocumentType = "Type 1", RequestingUser = "User 2", RequestDate = "20/07/2023", RequestStatus = "En attente", Action = "Valider" };
            DocumentRequest documentRequest3 = new DocumentRequest { Id = "3", DocumentType = "Type 2", RequestingUser = "User 3", RequestDate = "25/07/2023", RequestStatus = "En cours", Action = "Valider" };
            documentRequestDataGridView.Rows.Add(documentRequest1.Id, documentRequest1.DocumentType, documentRequest1.RequestingUser, documentRequest1.RequestDate, documentRequest1.RequestStatus, documentRequest1.Action);
            documentRequestDataGridView.Rows.Add(documentRequest2.Id, documentRequest2.DocumentType, documentRequest2.RequestingUser, documentRequest2.RequestDate, documentRequest2.RequestStatus, documentRequest2.Action);
            documentRequestDataGridView.Rows.Add(documentRequest3.Id, documentRequest3.DocumentType, documentRequest3.RequestingUser, documentRequest3.RequestDate, documentRequest3.RequestStatus, documentRequest3.Action);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 login = new Form1();
            login.Show();
        }
    }
}
