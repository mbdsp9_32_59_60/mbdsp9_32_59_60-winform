﻿namespace mbdsp9_32_59_60_WinForm
{
    partial class DocumentRequestList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DocumentRequestListLabel = new Label();
            searchPanel = new Panel();
            searchBtn = new Button();
            searchTextBox = new TextBox();
            tablePanel = new Panel();
            documentRequestDataGridView = new DataGridView();
            Id = new DataGridViewTextBoxColumn();
            DocumentType = new DataGridViewTextBoxColumn();
            RequestingUser = new DataGridViewTextBoxColumn();
            RequestDate = new DataGridViewTextBoxColumn();
            RequestStatus = new DataGridViewTextBoxColumn();
            Action = new DataGridViewButtonColumn();
            logoutBtn = new Button();
            searchPanel.SuspendLayout();
            tablePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)documentRequestDataGridView).BeginInit();
            SuspendLayout();
            // 
            // DocumentRequestListLabel
            // 
            DocumentRequestListLabel.AutoSize = true;
            DocumentRequestListLabel.Location = new Point(308, 41);
            DocumentRequestListLabel.Name = "DocumentRequestListLabel";
            DocumentRequestListLabel.Size = new Size(189, 15);
            DocumentRequestListLabel.TabIndex = 0;
            DocumentRequestListLabel.Text = "Liste des demandes de documents";
            // 
            // searchPanel
            // 
            searchPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            searchPanel.Controls.Add(searchBtn);
            searchPanel.Controls.Add(searchTextBox);
            searchPanel.Location = new Point(12, 78);
            searchPanel.Name = "searchPanel";
            searchPanel.Size = new Size(776, 71);
            searchPanel.TabIndex = 1;
            // 
            // searchBtn
            // 
            searchBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            searchBtn.Location = new Point(684, 20);
            searchBtn.Name = "searchBtn";
            searchBtn.Size = new Size(75, 23);
            searchBtn.TabIndex = 2;
            searchBtn.Text = "Rechercher";
            searchBtn.UseVisualStyleBackColor = true;
            // 
            // searchTextBox
            // 
            searchTextBox.Location = new Point(15, 20);
            searchTextBox.Name = "searchTextBox";
            searchTextBox.Size = new Size(660, 23);
            searchTextBox.TabIndex = 1;
            // 
            // tablePanel
            // 
            tablePanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            tablePanel.Controls.Add(documentRequestDataGridView);
            tablePanel.Location = new Point(12, 151);
            tablePanel.Name = "tablePanel";
            tablePanel.Size = new Size(776, 291);
            tablePanel.TabIndex = 2;
            tablePanel.Paint += tablePanel_Paint;
            // 
            // documentRequestDataGridView
            // 
            documentRequestDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            documentRequestDataGridView.Columns.AddRange(new DataGridViewColumn[] { Id, DocumentType, RequestingUser, RequestDate, RequestStatus, Action });
            documentRequestDataGridView.Dock = DockStyle.Fill;
            documentRequestDataGridView.Location = new Point(0, 0);
            documentRequestDataGridView.Name = "documentRequestDataGridView";
            documentRequestDataGridView.RowTemplate.Height = 25;
            documentRequestDataGridView.Size = new Size(776, 291);
            documentRequestDataGridView.TabIndex = 0;
            documentRequestDataGridView.CellContentClick += dataGridView1_CellContentClick;
            // 
            // Id
            // 
            Id.HeaderText = "ID";
            Id.Name = "Id";
            // 
            // DocumentType
            // 
            DocumentType.HeaderText = "Type du Document";
            DocumentType.Name = "DocumentType";
            DocumentType.Width = 150;
            // 
            // RequestingUser
            // 
            RequestingUser.HeaderText = "Demandeur";
            RequestingUser.Name = "RequestingUser";
            RequestingUser.Width = 250;
            // 
            // RequestDate
            // 
            RequestDate.HeaderText = "Date de demande";
            RequestDate.Name = "RequestDate";
            RequestDate.Width = 150;
            // 
            // RequestStatus
            // 
            RequestStatus.HeaderText = "Statut";
            RequestStatus.Name = "RequestStatus";
            // 
            // Action
            // 
            Action.HeaderText = "Action";
            Action.Name = "Action";
            Action.Text = "";
            // 
            // logoutBtn
            // 
            logoutBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            logoutBtn.Location = new Point(679, 6);
            logoutBtn.Name = "logoutBtn";
            logoutBtn.Size = new Size(110, 23);
            logoutBtn.TabIndex = 3;
            logoutBtn.Text = "Se deconnecter";
            logoutBtn.UseVisualStyleBackColor = true;
            logoutBtn.Click += button1_Click;
            // 
            // DocumentRequestList
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(logoutBtn);
            Controls.Add(tablePanel);
            Controls.Add(searchPanel);
            Controls.Add(DocumentRequestListLabel);
            MinimumSize = new Size(816, 489);
            Name = "DocumentRequestList";
            Text = "Gestion des demandes de documents";
            searchPanel.ResumeLayout(false);
            searchPanel.PerformLayout();
            tablePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)documentRequestDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label DocumentRequestListLabel;
        private Panel searchPanel;
        private Panel tablePanel;
        private Button searchBtn;
        private TextBox searchTextBox;
        private DataGridView documentRequestDataGridView;
        private DataGridViewTextBoxColumn DocumentType;
        private DataGridViewTextBoxColumn RequestingUser;
        private DataGridViewTextBoxColumn RequestDate;
        private DataGridViewTextBoxColumn RequestStatus;
        private DataGridViewButtonColumn Action;
        private DataGridViewTextBoxColumn Id;
        private Button logoutBtn;
    }
}