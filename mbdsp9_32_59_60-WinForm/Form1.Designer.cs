﻿namespace mbdsp9_32_59_60_WinForm
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            emailLabel = new Label();
            pwdLabel = new Label();
            emailTextBox = new TextBox();
            pwdTextBox = new TextBox();
            loginBtn = new Button();
            SuspendLayout();
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Location = new Point(12, 70);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new Size(36, 15);
            emailLabel.TabIndex = 0;
            emailLabel.Text = "Email";
            // 
            // pwdLabel
            // 
            pwdLabel.AutoSize = true;
            pwdLabel.Location = new Point(12, 133);
            pwdLabel.Name = "pwdLabel";
            pwdLabel.Size = new Size(77, 15);
            pwdLabel.TabIndex = 1;
            pwdLabel.Text = "Mot de passe";
            pwdLabel.Click += label2_Click;
            // 
            // emailTextBox
            // 
            emailTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            emailTextBox.Location = new Point(125, 62);
            emailTextBox.Name = "emailTextBox";
            emailTextBox.Size = new Size(337, 23);
            emailTextBox.TabIndex = 2;
            // 
            // pwdTextBox
            // 
            pwdTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            pwdTextBox.Location = new Point(125, 125);
            pwdTextBox.Name = "pwdTextBox";
            pwdTextBox.Size = new Size(337, 23);
            pwdTextBox.TabIndex = 3;
            // 
            // loginBtn
            // 
            loginBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            loginBtn.Location = new Point(178, 189);
            loginBtn.Name = "loginBtn";
            loginBtn.Size = new Size(134, 23);
            loginBtn.TabIndex = 4;
            loginBtn.Text = "Se connecter";
            loginBtn.UseVisualStyleBackColor = true;
            loginBtn.Click += loginBtn_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(474, 248);
            Controls.Add(loginBtn);
            Controls.Add(pwdTextBox);
            Controls.Add(emailTextBox);
            Controls.Add(pwdLabel);
            Controls.Add(emailLabel);
            MinimumSize = new Size(490, 287);
            Name = "Form1";
            Text = "e-gouvernance - Authentification";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label emailLabel;
        private Label pwdLabel;
        private TextBox emailTextBox;
        private TextBox pwdTextBox;
        private Button loginBtn;
    }
}