﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mbdsp9_32_59_60_WinForm.model
{
    public class DocumentRequest
    {
        public string Id { get; set; }
        public string DocumentType { get; set; }
        public string RequestingUser { get; set; }
        public string RequestDate { get; set; }
        public string RequestStatus { get; set; }
        public string Action { get; set; }
    }
}
